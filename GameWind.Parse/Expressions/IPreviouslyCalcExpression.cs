﻿using DynamicExpresso;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace GameWind.Parse.Expressions
{
    public interface IPreviouslyCalcExpression
    {
        /// <summary>
        /// Идентификатор выражения
        /// </summary>
        String Name { get; }

        /// <summary>
        /// Порядок, в котором будет вычисляться выражение
        /// </summary>
        Int32 CalcOrder { get; }

        String CalcString(params Object[] args);
        void Initialize(String name, params Parameter[] parameters);
        void UpdateExpressionFromMainExpressionText(String mainExpressionText, Boolean initCalcOrder = false);
        void UpdateExpression(String expressionText);
        Regex GetExpressionRegex();
    }
}
