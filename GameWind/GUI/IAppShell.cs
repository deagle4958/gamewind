﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameWind.GUI
{
    public interface IAppShell
    {
        String WindowTitle { get; set; }
    }
}
