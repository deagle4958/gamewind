﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace GameWind.GUI.Styles
{
    public partial class CustomWindowStyle : ResourceDictionary
    {
        public CustomWindowStyle()
        {
            InitializeComponent();
        }

        private void OnWindowButtonClick(object sender, RoutedEventArgs e)
        {
            if(sender is Button b && b.TemplatedParent is Window w )
            {
                switch (b.Name)
                {
                    case "MaximizeButton":
                        {
                            if(w.WindowState == WindowState.Normal)
                            {
                                w.WindowState = WindowState.Maximized;
                            }
                            else
                            {
                                w.WindowState = WindowState.Normal;
                            }
                            break;
                        }
                    case "MinimizeButton":
                        {
                            w.WindowState = WindowState.Minimized;
                            break;
                        }
                    case "CloseButton":
                        {
                            w.Close();
                            break;
                        }
                }
            }
        }
    }
}
