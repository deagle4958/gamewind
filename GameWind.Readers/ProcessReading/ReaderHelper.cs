﻿using Base.Game;
using Base.Process;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

namespace GameWind.Readers.ProcessReading
{
    public static class ReaderHelper
    {
        #region Handle

        /// <summary>
        /// Получение дескриптора процесса
        /// </summary>
        /// <param name="proc">Процесс</param>
        /// <param name="flags">Режим доступа (по умолчанию - открыть для чтения)</param>
        /// <returns></returns>
        public static IntPtr GetProcessHandle(this Process proc, ProcessAccessFlags flags = ProcessAccessFlags.VirtualMemoryRead)
        {
            return OpenProcess(flags, false, proc.Id);
        }

        /// <summary>
        /// Освободить дескриптор процесса
        /// </summary>
        /// <param name="handle">Дескриптор процесса</param>
        /// <returns></returns>
        public static Boolean CloseProcessHandle(this IntPtr handle)
        {
            return CloseHandle(handle);
        }

        #endregion

        #region ProcMemory

        /// <summary>
        /// Чтение памяти по заданному адресу
        /// </summary>
        /// <typeparam name="T">Тип значения в ячейке</typeparam>
        /// <param name="handle">Дескриптор процесса</param>
        /// <param name="address">Адрес</param>
        /// <returns></returns>
        public static T ReadProcMemory<T>(this IntPtr handle, IntPtr address)
        {
            var size = Marshal.SizeOf<T>();
            var buffer = new T[size];
            ReadProcessMemory(handle, address, buffer, size, out var readedBytesCount);
            return buffer.FirstOrDefault();
        }

        /// <summary>
        /// Чтение памяти с нахождением конечного адреса
        /// </summary>
        /// <typeparam name="T">Тип значения в ячейке</typeparam>
        /// <param name="handle">Дескриптор процесса</param>
        /// <param name="addressInfo">Базовый адрес и список смещений</param>
        /// <returns></returns>
        public static T ReadProcMemory<T>(this IntPtr handle, AddressInfo addressInfo)
        {
            var endAddress = addressInfo.BaseAddress;

            if (addressInfo.Offsets.Any())
            {
                endAddress = ReadProcMemory<IntPtr>(handle, addressInfo.BaseAddress);
                var tmpAddress = endAddress;

                if (addressInfo.Offsets.Count > 1)
                {
                    // список промежуточных смещений
                    // применив такое смещение получим новый адрес
                    // в "ячейке" по этому адресу будет находиться адрес следующего поинтера
                    var intermediateOffsets = addressInfo.Offsets
                        .Take(addressInfo.Offsets.Count - 1)
                        .Select(_ => _.Value);

                    foreach (var offset in intermediateOffsets)
                    {
                        var nextAddress = IntPtr.Add(tmpAddress, offset);
                        tmpAddress = ReadProcMemory<IntPtr>(handle, nextAddress);
                    }
                }

                var lastOffset = addressInfo.Offsets
                    .Last()
                    .Value;

                endAddress = IntPtr.Add(tmpAddress, lastOffset);
            }

            // конечный адрес по которому находится интересующее значение
            return ReadProcMemory<T>(handle, endAddress);
        }

        #endregion

        #region LowLevel

        [Flags]
        public enum ProcessAccessFlags : UInt32
        {
            /// <summary>
            /// Все возможные права доступа для объекта процесса
            /// </summary>
            All = 0x001F0FFF,

            /// <summary>
            /// Необходимо для закрытия процесса
            /// </summary>
            Terminate = 0x00000001,

            /// <summary>
            /// Необходимо для создания потока
            /// </summary>
            CreateThread = 0x00000002,

            /// <summary>
            /// Необходимо, чтобы выполнять операцию в адресном пространстве процесса
            /// </summary>
            VirtualMemoryOperation = 0x00000008,

            /// <summary>
            /// Необходимо, чтобы читать память в процессе, используя функцию <see cref="ReadProcessMemory"/>
            /// </summary>
            VirtualMemoryRead = 0x00000010,

            /// <summary>
            /// Необходимо, чтобы записывать в память процесса, используя функцию WriteProcessMemory (kernel32.dll)
            /// </summary>
            VirtualMemoryWrite = 0x00000020,

            /// <summary>
            /// Необходимо для дублирования дескриптора используемого функцией DuplicateHandle (kernel32.dll)
            /// </summary>
            DuplicateHandle = 0x00000040,

            /// <summary>
            /// Необходимо для создания процесса
            /// </summary>
            CreateProcess = 0x000000080,

            /// <summary>
            /// Необходимо, чтобы установить ограничения памяти, используемые функцией SetProcessWorkingSetSize (kernel32.dll)
            /// </summary>
            SetQuota = 0x00000100,

            /// <summary>
            /// Необходимо, чтобы установить некоторую информацию о процессе, такую как его класс приоритета
            /// </summary>
            SetInformation = 0x00000200,

            /// <summary>
            ///	Необходимо, чтобы извлечь некоторую информацию о процессе, такую как его код выхода и класс приоритета
            /// </summary>
            QueryInformation = 0x00000400,

            /// <summary>
            /// ///	Необходимо, чтобы извлечь некоторую информацию о процессе, такую как его код выхода и класс приоритета
            /// </summary>
            QueryLimitedInformation = 0x00001000,

            /// <summary>
            /// Необходимо, чтобы ждущий процесс завершил работу, используя функции ожидания
            /// </summary>
            Synchronize = 0x00100000
        }


        /// <summary>
        /// Получить дескриптор процесса
        /// </summary>
        /// <param name="processAccess">Режим открытия процесса</param>
        /// <param name="bInheritHandle">Возможность наследования дескриптора</param>
        /// <param name="processId">Идентификатор процесса</param>
        /// <returns></returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern IntPtr OpenProcess(
            ProcessAccessFlags processAccess,
            Boolean bInheritHandle,
            Int32 processId);

        /// <summary>
        /// Освободить дескриптор процесса
        /// </summary>
        /// <param name="handler">Дескриптор</param>
        /// <returns></returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
        [SuppressUnmanagedCodeSecurity]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern Boolean CloseHandle(IntPtr handler);

        /// <summary>
        /// Чтение памяти по заданному адресу
        /// </summary>
        /// <param name="handleProcess">Дескриптор процесса</param>
        /// <param name="lpBaseAddress">Адрес</param>
        /// <param name="lpBuffer">Буфер, в который вернётся результат чтения</param>
        /// <param name="dwSize">Число байт, которое читается из этого адреса</param>
        /// <param name="lpNumberOfBytesRead">Количество прочитанных байт</param>
        /// <returns></returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern Boolean ReadProcessMemory(
            IntPtr handleProcess,
            IntPtr lpBaseAddress,
            [Out] Byte[] lpBuffer,
            Int32 dwSize,
            out IntPtr lpNumberOfBytesRead);

        /// <summary>
        /// Чтение памяти по заданному адресу
        /// </summary>
        /// <param name="handleProcess">Дескриптор процесса</param>
        /// <param name="lpBaseAddress">Адрес</param>
        /// <param name="lpBuffer">Буфер, в который вернётся результат чтения</param>
        /// <param name="dwSize">Число байт, которое читается из этого адреса</param>
        /// <param name="lpNumberOfBytesRead">Количество прочитанных байт</param>
        /// <returns></returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern Boolean ReadProcessMemory(
            IntPtr handleProcess,
            IntPtr lpBaseAddress,
            [Out, MarshalAs(UnmanagedType.AsAny)] Object lpBuffer,
            Int32 dwSize,
            out IntPtr lpNumberOfBytesRead);

        /// <summary>
        /// Чтение памяти по заданному адресу
        /// </summary>
        /// <param name="handleProcess">Дескриптор процесса</param>
        /// <param name="lpBaseAddress">Адрес</param>
        /// <param name="lpBuffer">Буфер, в который вернётся результат чтения</param>
        /// <param name="dwSize">Число байт, которое читается из этого адреса</param>
        /// <param name="lpNumberOfBytesRead">Количество прочитанных байт</param>
        /// <returns></returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern Boolean ReadProcessMemory(
            IntPtr handleProcess,
            IntPtr lpBaseAddress,
            IntPtr lpBuffer,
            Int32 dwSize,
            out IntPtr lpNumberOfBytesRead);

        #endregion
    }
}
