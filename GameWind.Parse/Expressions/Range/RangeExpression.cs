﻿using DynamicExpresso;
using GameWind.Parse.Parsing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using static GameWind.Parse.Parsing.ParseConsts;
using static GameWind.Parse.Parsing.ParseConsts.RegExpressions;

namespace GameWind.Parse.Expressions.Range
{
    public class RangeExpression : PreviouslyCalcExpression, IPreviouslyCalcExpressionT<Double>
    {
        public RangeExpression()
        {
            _arduinoRange = new ArduinoRange();
        }

        public RangeExpression(String name, params Parameter[] parameters) : this()
        {
            Initialize(name, parameters);
        }

        public RangeExpression(String name, String expressionText, params Parameter[] parameters) : this()
        {
            Initialize(name, parameters);
            UpdateExpression(expressionText);
        }

        public override void UpdateExpression(String expression)
        {
            var interpreter = new Interpreter();
            var subExpressions = expression
                .Split(SubExpressionSeparators, StringSplitOptions.RemoveEmptyEntries)
                .Skip(1)
                .ToArray();

            _valueExpr = interpreter.Parse(subExpressions[0], _parameters);

            if (subExpressions.Length >= 3)
            {
                var inMinExprText = subExpressions[1];
                var inMaxExprText = subExpressions[2];

                _isOptionalInput =
                    !interpreter.SetLambda(inMinExprText, _parameters, out _inMinValueExpr) ||
                    !interpreter.SetLambda(inMaxExprText, _parameters, out _inMaxValueExpr);
            }

            if (subExpressions.Length == 5)
            {
                var outMinExprText = subExpressions[3];
                var outMaxExprText = subExpressions[4];

                interpreter.SetLambda(outMinExprText, _parameters, out _outMinValueExpr);
                interpreter.SetLambda(outMaxExprText, _parameters, out _outMaxValueExpr);
            }
        }

        public Double Calc(params Object[] args)
        {
            var value = _valueExpr.InvokeDouble(args);
            if (_inMinValueExpr != null)
            {
                InMin = _inMinValueExpr.InvokeDouble(args);
            }
            if (_inMaxValueExpr != null)
            {
                InMax = _inMaxValueExpr.InvokeDouble(args);
            }
            if (_outMinValueExpr != null)
            {
                OutMin = _outMinValueExpr.InvokeDouble(args);
            }
            if (_outMaxValueExpr != null)
            {
                OutMax = _outMaxValueExpr.InvokeDouble(args);
            }

            var outValue = _arduinoRange.Calc(value);
            return outValue;
        }

        public override String CalcString(params Object[] args)
        {
            return Calc(args).ToString();
        }

        public Double InMin
        {
            get => _arduinoRange.InMin;
            set => _arduinoRange.InMin = value;
        }

        public Double InMax
        {
            get => _arduinoRange.InMax;
            set => _arduinoRange.InMax = value;
        }

        public Double OutMin
        {
            get => _arduinoRange.OutMin;
            set => _arduinoRange.OutMin = value;
        }

        public Double OutMax
        {
            get => _arduinoRange.OutMax;
            set => _arduinoRange.OutMax = value;
        }

        public Boolean IsOptionalInput => _isOptionalInput;

        private Lambda _valueExpr = null;
        private Lambda _inMinValueExpr = null;
        private Lambda _inMaxValueExpr = null;
        private Lambda _outMinValueExpr = null;
        private Lambda _outMaxValueExpr = null;

        private ArduinoRange _arduinoRange;
        private Boolean _isOptionalInput;
    }
}
