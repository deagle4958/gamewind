﻿using GameWind.GUI;
using GameWind.GUI.Navigation;
using GameWind.MVVM;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameWind
{
    public class MainWindowViewModel : ViewModelBase, IAppShell
    {
        public String HeaderTitle
        {
            get => _headerTitle;
            set
            {
                _headerTitle = value;
                RaisePropertyChanged(() => HeaderTitle);
            }
        }

        public INavigationMenu NavigationMenu => _navigationMenu;

        public String WindowTitle
        {
            get => _windowTitle;
            set
            {
                _windowTitle = value;
                RaisePropertyChanged(() => WindowTitle);
            }
        }

        public MainWindowViewModel(INavigationMenu navigationMenu)
        {
            WindowTitle = HeaderTitle ="GameWindToolkit NEW DESIGN";
            _navigationMenu = navigationMenu;
        }


        private String _windowTitle;
        private String _headerTitle;
        private readonly INavigationMenu _navigationMenu;
    }
}
