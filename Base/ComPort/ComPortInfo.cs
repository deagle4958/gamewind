﻿using Base.Writing;
using System;
using System.Collections.Generic;
using System.Text;

namespace Base.ComPort
{
    public class ComPortDeviceInfo
    {
        public ComPortDeviceInfo(String deviceId, String name, String caption)
        {
            DeviceId = deviceId;
            Name = name;
            Caption = caption;

            PnpDeviceId = String.Empty;
            ProviderType = String.Empty;
            Status = String.Empty;
        }

        public override String ToString()
        {
            var stringBuilder = new StringBuilder();

            stringBuilder.AppendLine(DeviceId);
            stringBuilder.AppendLine(PnpDeviceId);
            stringBuilder.AppendLine(Name);
            stringBuilder.AppendLine(Caption);
            stringBuilder.AppendLine(ProviderType);
            stringBuilder.AppendLine(Status);
            stringBuilder.AppendLine("\r\n \r\n");

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Идентификатор устройства (COM...)
        /// </summary>
        public String DeviceId { get; set; }

        /// <summary>
        /// Уникальный идентификатор от MS
        /// </summary>
        public String PnpDeviceId { get; set; }

        /// <summary>
        /// Название устройства
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public String Caption { get; set; }

        /// <summary>
        /// Тип провайдера
        /// </summary>
        public String ProviderType { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public String Status { get; set; }
    }
}
