﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Storage
{
    public static class Storage
    {
        /// <summary>
        /// Получение элементов заданного типа
        /// </summary>
        /// <typeparam name="T">Тип</typeparam>
        /// <returns>Элементы</returns>
        public static List<T> GetItems<T>(Func<T, Boolean> selector = null) where T: IStorageItem
        {
            if (_items.TryGetValue(typeof(T), out var list))
            {
                return list
                    .OfType<T>()
                    .Where(_ => selector == null || selector(_))
                    .ToList();
            }
            return new List<T>();
        }

        /// <summary>
        /// Добавление элемента в хранилище
        /// </summary>
        /// <typeparam name="T">Тип</typeparam>
        /// <param name="item">Элемент</param>
        public static void AddItem<T>(T item) where T: IStorageItem
        {
            var type = typeof(T);
            if (_items.TryGetValue(type, out var list))
            {
                if (!list.Contains(item))
                {
                    list.Add(item);
                }
                else
                {
                    _logger.Warn($"Trying to add existing item on storage. Type {type.Name}");
                }
            }
            else
            {
                var newList = new List<IStorageItem>() { item };
                _items.Add(type, newList);
            }
        }

        /// <summary>
        /// Добавление элементов в хранилище
        /// </summary>
        /// <typeparam name="T">Тип элемента</typeparam>
        /// <param name="items">Элементы</param>
        public static void AddItems<T>(IEnumerable<T> items) where T : IStorageItem
        {
            var type = typeof(T);
            var itemsList = items
                .OfType<IStorageItem>()
                .ToList();

            if (_items.TryGetValue(type, out var list))
            {
                var exceptItems = list.Except(itemsList);
                list.AddRange(exceptItems);
            }
            else
            {
                _items.Add(type, itemsList);
            }
        }

        /// <summary>
        /// Замена элементов типа <typeparamref name="T"/> новыми
        /// </summary>
        /// <typeparam name="T">Тип</typeparam>
        /// <param name="items">Новые элементы</param>
        public static void SetItems<T>(IEnumerable<T> items) where T : IStorageItem
        {
            var type = typeof(T);
            var itemsList = items
                .OfType<IStorageItem>()
                .ToList();

            if (_items.TryGetValue(type, out var list))
            {
                list.Clear();
                list.AddRange(itemsList);
            }
            else
            {
                _items.Add(type, itemsList);
            }
        }

        /// <summary>
        /// Получение выбранного элемента заданного типа
        /// </summary>
        /// <typeparam name="T">Тип</typeparam>
        /// <returns>Выбранный элемент</returns>
        public static T GetSelectedItem<T>() where T: IStorageItem
        {
            var type = typeof(T);
            if (_selectedItems.ContainsKey(type))
            {
                return (T)_selectedItems[type];

            }
            else
            {
                return default(T);
            }
        }

        /// <summary>
        /// Установка выбранного элемента заданного типа
        /// </summary>
        /// <typeparam name="T">Тип</typeparam>
        /// <param name="item">Элемент</param>
        public static void SetSelectedItem<T>(T item) where T: IStorageItem
        {
            var type = typeof(T);
            if(_selectedItems.ContainsKey(type))
            {
                _selectedItems[type] = item;
            }
            else
            {
                _selectedItems.Add(type, item);
            }
        }

        public static void RemoveItems<T>(Func<T, Boolean> selector = null) where T : IStorageItem
        {
            if (_items.TryGetValue(typeof(T), out var list))
            {
                list.RemoveAll(_ => selector == null || selector((T)_));
            }
        }

        /// <summary>
        /// Основное хранилище
        /// </summary>
        private static Dictionary<Type, List<IStorageItem>> _items { get; } = new Dictionary<Type, List<IStorageItem>>();

        /// <summary>
        /// Хранилище выбранных элементов
        /// </summary>
        private static Dictionary<Type, IStorageItem> _selectedItems { get; } = new Dictionary<Type, IStorageItem>();

        private static readonly ILogger _logger = LogManager.GetCurrentClassLogger();
    }
}
