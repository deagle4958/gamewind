﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace Base.Process
{
    public class Offset
    {
        public Offset()
        {
        }

        public Offset(Int32 value)
        {
            Value = value;
        }

        public Offset(String hexValue)
        {
            HexValue = hexValue;
        }


        /// <summary>
        /// Значение
        /// </summary>
        public Int32 Value { get; set; }

        /// <summary>
        /// Значение в HEX
        /// </summary>
        public String HexValue
        {
            get { return Value.ToString(_hexSpecifier); }
            set { Value = Int32.Parse(value, NumberStyles.HexNumber); }
        }

        public const String _hexSpecifier = "X";
    }
}
