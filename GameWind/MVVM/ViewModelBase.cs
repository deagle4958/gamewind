﻿using Base.ViewModels;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace GameWind.MVVM
{
    public abstract class ViewModelBase : BindableBase, IViewModel, IWidget
    {
        public String Header
        {
            get => _header;
            protected set
            {
                _header = value;
                RaisePropertyChanged(() => Header);
            }
        }

        public void RaisePropertyChanged<T>(Expression<Func<T>> expression)
        {
            var memberExpression = expression.Body as MemberExpression;
            var memberName = memberExpression?.Member?.Name;
            if(memberName != null)
            {
                RaisePropertyChanged(memberName);
            }
        }

        private String _header;
    }
}
