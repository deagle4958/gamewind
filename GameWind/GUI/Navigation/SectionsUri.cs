﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameWind.GUI.Navigation
{
    public static class SectionsUri
    {
        public const String TestSection = nameof(TestSection);
        public const String TestView1 = nameof(TestView1);

        public static Uri TestSectionUri = new Uri($"\\{TestSection}", UriKind.Relative);
        public static Uri TestView1Uri = new Uri($"\\{TestView1}", UriKind.Relative);
    }
}
