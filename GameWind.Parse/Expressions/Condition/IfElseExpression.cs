﻿using DynamicExpresso;
using GameWind.Parse.Parsing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using static GameWind.Parse.Parsing.ParseConsts;
using static GameWind.Parse.Parsing.ParseConsts.RegExpressions;

namespace GameWind.Parse.Expressions.Condition
{
    public class IfElseExpression : PreviouslyCalcExpression, IPreviouslyCalcExpressionT<String>
    {
        public IfElseExpression()
        {
        }

        public IfElseExpression(String name, params Parameter[] parameters) : this()
        {
            Initialize(name, parameters);
        }

        public IfElseExpression(String name, String expressionText, params Parameter[] parameters) : this()
        {
            Initialize(name, parameters);
            UpdateExpression(expressionText);
        }

        public override void UpdateExpression(String expression)
        {
            var interpreter = new Interpreter();
            _conditionExprText = SimpleBracketsRegex
               .Match(expression)
               .Result("$1")
               .Replace("[", String.Empty)
               .Replace("]", String.Empty)
               .Replace("\r\n", String.Empty);

            _conditionExpr = interpreter.Parse(_conditionExprText, _parameters);

            var returnExpressions = CurlyBracketsRegex
                .Matches(expression)
                .Cast<Match>()
                .Select(_ => _
                    .Result("$1")
                    .Replace("\r\n", String.Empty)
                    .Trim())
                .ToArray();

            _trueExprText = returnExpressions[0];

            if (returnExpressions.Length > 1)
            {
                _falseExprText = returnExpressions[1];
            }
        }

        public override Int32 GetCalcOrder(String expression)
        {
            var conditionCalcOrder = SubExpressionsRegex.Matches(_conditionExprText).Count;
            var trueExprCalcOrder = SubExpressionsRegex.Matches(_trueExprText).Count;
            var falseExprCalcOrder = 0;
            if (!String.IsNullOrEmpty(_falseExprText))
            {
                falseExprCalcOrder = SubExpressionsRegex.Matches(_conditionExprText).Count;
            }

            return Enumerable.Max(new[]
            {
                conditionCalcOrder,
                trueExprCalcOrder,
                falseExprCalcOrder,
            });
        }

        public String Calc(params Object[] args)
        {
            var conditionResult = _conditionExpr.InvokeBoolean(args);
            if (conditionResult)
            {
                return TrueExprText;
            }
            return FalseExprText ?? String.Empty;
        }

        public override String CalcString(params Object[] args)
        {
            return Calc(args);
        }

        public String TrueExprText => _trueExprText;
        public String FalseExprText => _falseExprText;

        private Lambda _conditionExpr;

        private String _conditionExprText = null;
        private String _trueExprText = null;
        private String _falseExprText = null;
    }
}
