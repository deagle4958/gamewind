﻿using Base.Game;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameWind.Readers.ProcessReading;
using Base.Reading;
using Base.Process;

namespace GameWind.Readers.ProcessReading
{
    public class ProcessReader : IReader<AddressInfo>, IDisposable
    {
        /// <summary>
        /// Конструктор ридера
        /// </summary>
        /// <param name="processName">Имя процесса</param>
        public ProcessReader(String processName)
        {
            var process = Process
               .GetProcessesByName(processName)
               .FirstOrDefault();

            if (process == null)
            {
                throw new ArgumentNullException("Process", $"Process with name {processName} was not found");
            }

            _handle = process.GetProcessHandle();
        }

        public void Dispose()
        {
            if(_handle != null)
            {
                _handle.CloseProcessHandle();
            }
        }

        public T Read<T>(AddressInfo address)
        {
            var memoryValue = _handle.ReadProcMemory<T>(address);
            return memoryValue;
        }

        private readonly IntPtr _handle;
    }
}
