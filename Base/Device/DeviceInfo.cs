﻿using Base.Game;
using Base.Storage;
using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Device
{
    public class DeviceInfo : IStorageItem
    {
        public DeviceInfo()
        {
            Name = String.Empty;
            Description = String.Empty;
        }

        public DeviceInfo(String name, String description) : this()
        {
            Name = name;
            Description = description;
        }

        /// <summary>
        /// Название
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public String Description { get; set; }
    }
}
