﻿using GameWind.GUI;
using GameWind.GUI.Navigation;
using GameWind.GUI.Settings;
using GameWind.Test;
using MaterialDesignThemes.Wpf;
using Prism.Ioc;
using Prism.Mvvm;
using Prism.Regions;
using Prism.Unity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace GameWind
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : PrismApplication
    {
        protected override Window CreateShell()
        {
            var ph = new PaletteHelper();
            ph.SetTheme(AppTheme.DefaultTheme);
            return this.Container.Resolve<MainWindow>();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry
                .Register<MainWindowViewModel>()                
                .Register<INavigationMenu, NavigationMenu>()
                .RegisterSingleton<PaletteHelper>();

            containerRegistry.RegisterForNavigation<TestSection>(SectionsUri.TestSection);
            containerRegistry.RegisterForNavigation<TestView1>(SectionsUri.TestView1);
        }

        protected override void ConfigureViewModelLocator()
        {
            base.ConfigureViewModelLocator();

            ViewModelLocationProvider.Register<MainWindow, MainWindowViewModel>();
            ViewModelLocationProvider.Register<SettingsView, SettingsViewModel>();
            ViewModelLocationProvider.Register<TestView, TestViewModel>();
            ViewModelLocationProvider.Register<TestView1, TestViewModel1>();
        }
    }
}
