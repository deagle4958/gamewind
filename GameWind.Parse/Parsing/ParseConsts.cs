﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace GameWind.Parse.Parsing
{
    public static class ParseConsts
    {
        public static class RegExpressions
        {
            /// <summary>
            /// Example:
            /// <c>
            ///     <para>[MaxSpeed]</para>
            /// </c>
            /// </summary>
            //
            public static Regex VariableRegex => new Regex("\\[(\\w+)\\]");

            /// <summary>
            /// Examples:
            /// <c>
            /// <list type="bullet">
            ///     <item>Range ([Speed])</item>
            ///     <item>Range ([Speed], 0, [MaxSpeed])</item>
            ///     <item>Range ([Speed], 0, [MaxSpeed], [ArduinoMinValue], [ArduinoMaxValue])</item>
            ///     <item>Range ([Speed], _, _, [ArduinoMinValue], [ArduinoMaxValue])</item>
            /// </list>
            /// </c>
            /// </summary>
            //  Range *\\(                                                                  Range (
            //   *[][\\w]+ *                                                                [Speed]
            //   *[][\\w]+ *, *[][\\w^_]+ *, *[][\\w^_]+                                    [Speed], 0, [MaxSpeed]
            //  ( *, *[][\\w]+ *, *[][\\w]+ *)?                                             [ArduinoMinValue], [ArduinoMaxValue]
            //  \\)                                                                         )
            public static Regex RangeRegex => new Regex("Range *\\(( *[][\\w]+ *| *[][\\w]+ *, *[][\\w^_]+ *, *[][\\w^_]+( *, *[][\\w]+ *, *[][\\w]+ *)?)\\)");

            /// <summary>
            /// Examples:
            /// <c>
            /// <list type="bullet">
            ///     <item>
            ///         <para>If ([SpeedKph] &gt; 400)</para>
            ///         <para>{ Range ([SpeedKph], 0, 250) }</para>
            ///     </item>
            ///     <item>
            ///         <para>If ([SpeedKph] &lt; [MaxSpeed])</para>
            ///         <para>{</para>
            ///         <para>[SpeedKph]</para>
            ///         <para>}</para> else { 100 }
            ///     </item>
            /// </list>
            /// </c>
            /// </summary>
            //  If\\s*\\(\\s*[][\\w<>!=&|\\s]+\\s*\\)                                       If ([SpeedKph] < [MaxSpeed])
            //  \\s*{[\\S\\s]*}                                                             { Range ([SpeedKph], 0, 250) }
            //  (\\s*else\\s*{[\\S\\s]*})?                                                  else { Range (SpeedKph], 0, 400) }
            public static Regex IfElseRegex => new Regex("If\\s*\\(\\s*[][\\w<>!=&|\\s]+\\s*\\)\\s*{((?!if)|(?!else)[\\S\\s])*}(\\s*else\\s*{[\\S\\s]*})?");

            /// <summary>
            /// Examples:
            /// <c>
            /// <list type="bullet">
            ///     <item>{ [Speed] + 5 }</item>
            ///     <item>
            ///         <para>{</para>
            ///         <para>Range ([Speed], 0, 400)</para>
            ///         <para>}</para>
            ///     </item>
            /// </list>
            /// </c>
            /// </summary>
            public static Regex CurlyBracketsRegex => new Regex("\\{([^\\{^\\}]+)\\}");

            /// <summary>
            /// Example:
            /// <c>
            /// <list type="bullet">
            ///     <item>( [Speed] &gt; 400 )</item>
            /// </list>
            /// </c>
            /// </summary>
            public static Regex SimpleBracketsRegex => new Regex("\\(([^\\(^\\)]+)\\)");

            /// <summary>
            /// Example:
            /// <c>
            /// <list type="bullet">
            ///     <item>$&lt;R0Range ([Speed])R0&gt;$</item>
            /// </list>
            /// </c>
            /// </summary>
            public static Regex SubExpressionsRegex => new Regex($"{ExpressionStartRegex}[^{ExpressionEndRegex}]+{ExpressionEndRegex}");
        }

        /// <summary>
        /// Разделитель выражений
        /// </summary>
        public const Char ExpressionSeparator = ';';

        /// <summary>
        /// Разделители отдельных частей выражения
        /// </summary>
        public static readonly Char[] SubExpressionSeparators = new Char[] { ' ', ',', '(', ')', '[', ']', '\r', '\n' };

        public const String SubExpressionIdentifier = "$";
        public const String SubExpressionStartIdentifier = "<";
        public const String SubExpressionEndIdentifier = ">";

        /// <summary>
        /// Начало подвыражения в тексте промежуточного выражения
        /// </summary>
        public static readonly String ExpressionStart = $"{SubExpressionIdentifier}{SubExpressionStartIdentifier}";

        /// <summary>
        /// Конец подвыражения в тексте промежуточного выражения
        /// </summary>
        public static readonly String ExpressionEnd = $"{SubExpressionEndIdentifier}{SubExpressionIdentifier}";

        /// <summary>
        /// Экранированный для применения в регулярном выражении текст - начало подвыражения
        /// </summary>
        public static readonly String ExpressionStartRegex = $"\\{SubExpressionIdentifier}{SubExpressionStartIdentifier}";

        /// <summary>
        /// Экранированный для применения в регулярном выражении текст - конец подвыражения
        /// </summary>
        public static readonly String ExpressionEndRegex = $"{SubExpressionEndIdentifier}\\{SubExpressionIdentifier}";

        public static class Identifiers
        {
            /// <summary>
            /// Идентификатор диапазонов в тексте промежуточного выражения
            /// </summary>
            public const String RangeIdentifier = "R";

            /// <summary>
            /// Идентификатор условия "If Else в тексте промежуточного выражения
            /// </summary>
            public const String IfElseIdentifier = "C";
        }
    }
}