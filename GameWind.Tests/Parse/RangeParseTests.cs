using DynamicExpresso;
using GameWind.Parse.Expressions.Range;
using NUnit.Framework;
using System;

namespace GameWind.Tests.Parse
{
    public class RangeParseTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void RangeExpressionTest()
        {
            var rangeExpr1 = new RangeExpression("Test", "Range ([Speed], 0, [MaxSpeed])", _parameters);
            var rangeExpr2 = new RangeExpression("Test", "Range ([Speed], 0, [MaxSpeed], 0, [ArduinoMaxValue])", _parameters);
            var rangeExpr3 = new RangeExpression("Test", "Range ([Speed], _, _, 0, [ArduinoMaxValue])", _parameters);
            rangeExpr3.InMin = 0;
            rangeExpr3.InMax = 200;
            var rangeExpr4 = new RangeExpression("Test", "Range ([Speed])", _parameters);
            rangeExpr4.InMin = 0;
            rangeExpr4.InMax = 200;

            var result1 = rangeExpr1.Calc(10, 100, 0, 0);
            var result2 = rangeExpr2.Calc(20, 100, 50, 0);
            var result3 = rangeExpr3.Calc(20, 100, 50, 0);
            var result4 = rangeExpr4.Calc(20, 100, 50, 0);

            Assert.AreEqual(25, result1);
            Assert.AreEqual(10, result2);
            Assert.AreEqual(5, result3);
            Assert.AreEqual(25, result4);
        }

        [Test]
        public void RangeCalcTest()
        {
            var range = new Base.Expressions.Range(0, 50, 0, 500);
            Assert.AreEqual(0, range.Calc(-10));
            Assert.AreEqual(100, range.Calc(10));
            Assert.AreEqual(500, range.Calc(50));
            Assert.AreEqual(500, range.Calc(51));
        }


        private readonly Parameter[] _parameters = new[]
        {
            new Parameter("Speed", typeof(Double)),
            new Parameter("MaxSpeed", typeof(Double)),
            new Parameter("ArduinoMaxValue", typeof(Double)),
            new Parameter("ElapsedSeconds", typeof(Double)),
        };
    }
}