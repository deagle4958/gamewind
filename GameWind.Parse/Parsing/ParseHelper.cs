﻿using DynamicExpresso;
using GameWind.Parse.Expressions;
using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace GameWind.Parse.Parsing
{
    public static class ParseHelper
    {
        public static Expression[] Parse(String text)
        {
            var expressions = text
                .Split(ParseConsts.ExpressionSeparator)
                .Select(_=> new Expression(_.Trim()))
                .ToArray();

            return expressions;
        }

        /// <summary>
        /// <para>Записывает в <paramref name="expression"/> выражение, полученное при парсинге</para>
        /// <para>Возвращает true, если <paramref name="expressionText"/> действительно содержит выражение</para>
        /// </summary>
        /// <param name="interpreter">Интерпретатор</param>
        /// <param name="expressionText">Текст выражения</param>
        /// <param name="parameters">Список параметров</param>
        /// <param name="expression">Готовое выражение</param>
        public static Boolean SetLambda(this Interpreter interpreter, String expressionText, Parameter[] parameters, out Lambda expression)
        {
            if (expressionText != "_")
            {
                expression = interpreter.Parse(expressionText, parameters);
                return true;
            }

            expression = null;
            return false;
        }

        public static Double InvokeDouble(this Lambda expression, params Object[] parameters)
        {
            var value = expression.Invoke(parameters);
            return Convert.ToDouble(value);
        }

        public static Boolean InvokeBoolean(this Lambda expression, params Object[] parameters)
        {
            var value = expression.Invoke(parameters);
            return Convert.ToBoolean(value);
        }
    }
}
