﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameWind.GUI.Navigation
{
    public interface INavigationMenuItem
    {
        String SectionName { get; }

        void ShowSection();
    }
}
