﻿using GameWind.Parse.Expressions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameWind.Tests.Parse
{
    public class ExpressionCalcTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void IfElseExpressionCalcTest()
        {
            var exprText = @"
            If ([Speed] < 400)
            {
                400 * 5
            }
            else
            {
                110 + 25
            }";
            var expression = new Expression(exprText);
            var result1 = expression.Calc(100);
            var result2 = expression.Calc(450);

            Assert.AreEqual(2000, result1);
            Assert.AreEqual(135, result2);
        }

        [Test]
        public void RangeExpressionCalcTest()
        {
            var exprText = @"Range ([Speed], 0, [MaxSpeed], 0, [ArduinoMaxValue])";
            var expression = new Expression(exprText);
            var result1 = expression.Calc(20, 100, 50);
            var result2 = expression.Calc(450, 500, 200);

            Assert.AreEqual(10, result1);
            Assert.AreEqual(180, result2);
        }

        [Test]
        public void ExpressionCalcTest()
        {
            var exprText = @"
            If ([Speed] < 400)
            {
                Range ([Speed], 0, [MaxSpeed], 0, [ArduinoMaxValue])
            }
            else
            {
                Range ([Speed], 0, 500, 0, 100)
            }";
            var expression = new Expression(exprText);
            var result1 = expression.Calc(100, 500, 200);
            var result2 = expression.Calc(450, 500, 200);

            Assert.AreEqual(40, result1);
            Assert.AreEqual(90, result2);
        }

        // TODO заменить использование регулярок транслятором
        // (если когда-нибудь появится необходимость использования вложенных выражений)
        //[Test]
        //public void ExpressionOrderCalcTest()
        //{
        //    var exprText = @"
        //    If ([Speed] < 400)
        //    {
        //        If ([Speed] > 200)
        //        {
        //            500
        //        }
        //        else
        //        {
        //            [Speed]
        //        }
        //    }
        //    else
        //    {
        //        Range ([Speed], 0, 500, 0, 100)
        //    }";
        //    var expression = new Expression(exprText);
        //    var result1 = expression.Calc(100);
        //    var result2 = expression.Calc(450);
        //    var result3 = expression.Calc(250);

        //    Assert.AreEqual(100, result1);
        //    Assert.AreEqual(90, result2);
        //    Assert.AreEqual(500, result3);
        //}

        [Test]
        public void IfElseCascadeExpressionTest()
        {
            var exprText = @"
            If ([Speed] < [MaxSpeed])
            {
                100
            }
            If ([Speed] == [MaxSpeed])
            {
                200
            }
            If ([Speed] > [MaxSpeed])
            {
                300
            }";
            var expr = new Expression(exprText);

            var result1 = expr.Calc(50, 100);
            var result2 = expr.Calc(100, 100);
            var result3 = expr.Calc(150, 100);

            Assert.AreEqual(100, result1);
            Assert.AreEqual(200, result2);
            Assert.AreEqual(300, result3);
        }
    }
}
