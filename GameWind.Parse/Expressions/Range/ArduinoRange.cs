﻿using Base.Expressions;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameWind.Parse
{
    public class ArduinoRange : Range
    {
        public ArduinoRange() : base()
        {
            OutMin = ArduinoMin;
            OutMax = ArduinoMax;
        }

        public ArduinoRange(Double inMin, Double inMax) : base(inMin, inMax)
        {
            OutMin = ArduinoMin;
            OutMax = ArduinoMax;
        }

        public ArduinoRange(Double inMin, Double inMax, Double outMin, Double outMax) : base(inMin, inMax, outMin, outMax)
        {
            if (outMin < ArduinoMin ||
                outMax > ArduinoMax)
            {
                throw new ArgumentException($"Arduino out parameters should be in range between {ArduinoMin} and {ArduinoMax}");
            }
        }

        public new Byte Calc(Double value)
        {
            var calcValue = base.Calc(value);
            return (Byte)calcValue;
        }

        public const Byte ArduinoMin = Byte.MinValue;
        public const Byte ArduinoMax = Byte.MaxValue;
    }
}
