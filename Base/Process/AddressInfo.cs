﻿using Base.Reading;
using Base.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Process
{
    public class AddressInfo : IStorageItem, IReadInfo
    {
        public AddressInfo()
        {
            Offsets = new List<Offset>();
        }

        public AddressInfo(IntPtr baseAddress) : this()
        {
            BaseAddress = baseAddress;
        }

        public AddressInfo(IntPtr baseAddress, params Int32[] offsets) : this(baseAddress)
        {
            Offsets = offsets
                .Select(_=> new Offset(_))
                .ToList();
        }

        public AddressInfo(IntPtr baseAddress, params String[] offsets) : this(baseAddress)
        {
            Offsets = offsets
                .Select(_ => new Offset(_))
                .ToList();
        }

        /// <summary>
        /// Комментарий
        /// </summary>
        public String Comment { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public String Description { get; set; }

        /// <summary>
        /// Базовый адрес (без учёта смещений)
        /// </summary>
        public IntPtr BaseAddress { get; set; }

        /// <summary>
        /// Массив смещений
        /// </summary>
        public List<Offset> Offsets { get; set; }

        /// <summary>
        /// Тип значения в области памяти
        /// </summary>
        public Type ValueType { get; set; }

        public override bool Equals(object obj)
        {
            // TODO
            return obj is AddressInfo info &&
                   EqualityComparer<IntPtr>.Default.Equals(BaseAddress, info.BaseAddress) &&
                   EqualityComparer<List<Offset>>.Default.Equals(Offsets, info.Offsets) &&
                   ValueType == info.ValueType;
        }

        public override int GetHashCode()
        {
            // TODO
            var hashCode = 1284155717;
            hashCode = hashCode * -1521134295 + EqualityComparer<IntPtr>.Default.GetHashCode(BaseAddress);
            hashCode = hashCode * -1521134295 + EqualityComparer<List<Offset>>.Default.GetHashCode(Offsets);
            hashCode = hashCode * -1521134295 + ValueType.GetHashCode();
            return hashCode;
        }

        public enum Type
        {
            Int16,
            Int32,
            Int64,
            Single,
            Double,
        }
    }
}
