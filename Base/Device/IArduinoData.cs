﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Device
{
    public interface IArduinoData
    {
        Dictionary<Byte, Byte?> SetAnalogData(Byte pin, Byte? data);
        Dictionary<Byte, Byte?> SetAnalogData(KeyValuePair<Byte, Byte?> data);

        Dictionary<Byte, Boolean?> SetDigitalData(Byte pin, Boolean? data);
        Dictionary<Byte, Boolean?> SetDigitalData(KeyValuePair<Byte, Boolean?> data);

        Byte[] GetAnalogPins();
        Byte[] GetDigitalPins();

        /// <summary>
        /// Аналоговые данные. Key: номер порта Arduino, Value: данные
        /// </summary>
        Dictionary<Byte, Byte?> AnalogData { get; }

        /// <summary>
        /// Цифровые данные. Key: номер порта Arduino, Value: данные
        /// </summary>
        Dictionary<Byte, Boolean?> DigitalData { get; }
    }
}
