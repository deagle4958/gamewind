﻿using Base.ComPort;
using System;
using System.Collections.Generic;
using System.Management;
using System.Text;

namespace GameWind.Writers.ComPortWriting
{
    public static class PortHelper
    {
        public static ComPortDeviceInfo[] GetAvailableComPorts()
        {
            var searcher = new ManagementObjectSearcher(_portsQuery);
            var ports = searcher.Get();

            var result = new List<ComPortDeviceInfo>(ports.Count);
            foreach (var port in ports)
            {
                var info = new ComPortDeviceInfo(
                    port["DeviceId"].ToString(),
                    port["Name"].ToString(),
                    port["Caption"].ToString())
                {
                    PnpDeviceId = port["PNPDeviceID"].ToString(),
                    ProviderType = port["ProviderType"].ToString(),
                    Status = port["Status"].ToString(),
                };

                result.Add(info);
            };

            return result.ToArray();
        }

        private const String _portsQuery = "Select * from Win32_SerialPort";
    }
}
