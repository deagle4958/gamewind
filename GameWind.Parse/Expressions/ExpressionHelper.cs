﻿using DynamicExpresso;
using GameWind.Parse.Expressions.Condition;
using GameWind.Parse.Expressions.Range;
using GameWind.Parse.Parsing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using static GameWind.Parse.Parsing.ParseConsts;

namespace GameWind.Parse.Expressions
{
    public static class ExpressionHelper
    {
        public static IEnumerable<T> CreateAll<T>(Regex regex, String identifier, Parameter[] parameters, ref String text, Boolean initExpr = false) where T : IPreviouslyCalcExpression, new()
        {
            var results = regex
            .Matches(text)
            .Cast<Match>()
            .Select((_, counter) => CreateT<T>(_.Value, identifier, parameters, counter, initExpr))
            .ToArray();

            var i = 0;
            text = regex
                .Replace(text, _ => $"{ExpressionStart}{identifier}{i}{_.Value}{identifier}{i++}{ExpressionEnd}");

            return results;
        }

        private static T CreateT<T>(String expr, String identifier, Parameter[] parameters, Int32 count, Boolean initExpr = false) where T : IPreviouslyCalcExpression, new()
        {
            var result = new T();
            result.Initialize($"{identifier}{count}", parameters);
            if (initExpr)
            {
                result.UpdateExpressionFromMainExpressionText(expr);
            }
            return result;
        }
    }
}
