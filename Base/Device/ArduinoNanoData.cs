﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Base.Device
{
    public class ArduinoNanoData : IArduinoData
    {
        public ArduinoNanoData()
        {
        }

        public Dictionary<Byte, Byte?> SetAnalogData(KeyValuePair<Byte, Byte?> data)
        {
            return SetAnalogData(data.Key, data.Value);
        }

        public Dictionary<Byte, Boolean?> SetDigitalData(KeyValuePair<Byte, Boolean?> data)
        {
            return SetDigitalData(data.Key, data.Value);
        }

        public Dictionary<Byte, Byte?> SetAnalogData(Byte pin, Byte? data)
        {
            if (AnalogData.ContainsKey(pin))
            {
                AnalogData[pin] = data;
            }
            else
            {
                _logger.Warn($"Trying to set analog data to incorrect pin. Pin: {pin}");
            }
            return AnalogData;
        }

        public Dictionary<Byte, Boolean?> SetDigitalData(Byte pin, Boolean? data)
        {
            if (DigitalData.ContainsKey(pin))
            {
                DigitalData[pin] = data;
            }
            else
            {
                _logger.Warn($"Trying to set digital data to incorrect pin. Pin: {pin}");
            }
            return DigitalData;
        }

        public Byte[] GetAnalogPins()
        {
            return AnalogData.Keys.ToArray();
        }

        public Byte[] GetDigitalPins()
        {
            return DigitalData.Keys.ToArray();
        }

        public Dictionary<Byte, Byte?> AnalogData { get; } = new Dictionary<Byte, Byte?>()
        {
            {3, null },
            {5, null },
            {6, null },
            {9, null },
            {10, null },
            {11, null },
        };

        public Dictionary<Byte, Boolean?> DigitalData { get; } = new Dictionary<Byte, Boolean?>()
        {
            {2, null },
            {4, null },
            {7, null },
            {8, null },
            {12, null },
            {13, null },
        };

        private readonly ILogger _logger = LogManager.GetCurrentClassLogger();
    }
}
