﻿using Base.Reading;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GameWind.Readers
{
    public interface IReader<Info> where Info : class, IReadInfo
    {
        T Read<T>(Info readInfo = null);
    }
}
