﻿using Base.Device;
using Base.File;
using Base.Game;
using Base.Process;
using Base.Storage;
using GameWind.Readers;
using GameWind.Readers.JsonFileReading;
using GameWind.Readers.ProcessReading;
using GameWind.Writers.ComPortWriting;
using GameWind.Writers.JsonFileWriting;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameWind.WinForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            _keysListBoxes = new ListBox[]
            {
                listBox5,
                listBox7,
            };

            _gamesListBoxes = new ListBox[]
            {
                listBox1,
                listBox2,
            };

            _devicesListBoxes = new ListBox[]
            {
                listBox3,
                listBox6,
            };

            _deviceKeysListBoxes = new ListBox[]
            {
                listBox8,
            };

        }

        #region Игра

        private void button2_Click(object sender, EventArgs e)
        {
            Storage.AddItem(new GameInfo() { DisplayingName = textBox4.Text, ProcessName = textBox3.Text });
            UpdateGamesList();
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        #endregion

        #region Ключи данных

        private void button9_Click(object sender, EventArgs e)
        {
            var key = new KeyString()
            {
                Key = textBox7.Text,
                Aliases = textBox8.Text
                    .Split(',', '\n')
                    .ToList()
            };

            Storage.AddItem(key);

            UpdateKeysList();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            using (var dialog = new SaveFileDialog())
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    var keys = Storage.GetItems<KeyString>();

                    var fileInfo = new SourceFileInfo() { Path = dialog.FileName };
                    using (var writer = new JsonFileWriter(fileInfo))
                    {
                        writer.WriteAll(keys);
                    }

                    UpdateKeysList();
                }
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            using (var dialog = new OpenFileDialog())
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    var fileInfo = new SourceFileInfo() { Path = dialog.FileName };
                    using (var reader = new JsonFileReader(fileInfo))
                    {
                        var keys = reader.ReadAll<KeyString>();
                        Storage.SetItems(keys);
                    }

                    UpdateKeysList();
                }
            }
        }

        private void listBox5_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox7.Text = (listBox5.SelectedItem as KeyString).Key;
            textBox8.Text = String.Join(Environment.NewLine, (listBox5.SelectedItem as KeyString).Aliases);
        }

        #endregion

        #region Адрес

        private void button1_Click(object sender, EventArgs e)
        {
            var baseAddressText = textBox1.Text;
            var offsetsText = textBox2.Text.Split(new Char[] { ',', '\n' }, StringSplitOptions.RemoveEmptyEntries);

            if (String.IsNullOrEmpty(baseAddressText))
            {
                MessageBox.Show("Нужно заполнить базовый адрес");
                return;
            }

            try
            {
                var baseAddress = (IntPtr)Int32.Parse(baseAddressText, System.Globalization.NumberStyles.HexNumber);

                var addressInfo = new AddressInfo(baseAddress, offsetsText);
                Storage.AddItem(addressInfo);

                var selectedGame = Storage.GetSelectedItem<GameInfo>();
                var selectedKey = treeView1.SelectedNode.Text;

                selectedGame.SetOrAddAddress(selectedKey, addressInfo);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            //LoadListsData(true);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            var games = Storage.GetItems<GameInfo>();
            using (var writer = new JsonFileWriter(new SourceFileInfo() { Path = _gamesFileName }))
            {
                writer.WriteAll(games);
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Storage.SetSelectedItem(listBox1.SelectedItem as GameInfo);

            if (treeView1.SelectedNode != null)
            {
                var game = listBox1.SelectedItem as GameInfo;
                var key = treeView1.SelectedNode.Text;

                if (game != null &&
                    key != null &&
                    game.GameAddresses.TryGetValue(key, out var addressInfo))
                {
                    textBox1.Text = addressInfo.BaseAddress.ToString("X");
                    textBox2.Text = String.Join(Environment.NewLine, addressInfo.Offsets.Select(_ => _.HexValue));
                }
                else
                {
                    textBox1.Text = String.Empty;
                    textBox2.Text = String.Empty;
                }
            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (treeView1.SelectedNode != null)
            {
                var game = Storage.GetSelectedItem<GameInfo>();
                var key = treeView1.SelectedNode.Text;

                if (game != null &&
                    key != null &&
                    game.GameAddresses.TryGetValue(key, out var addressInfo))
                {
                    textBox1.Text = addressInfo.BaseAddress.ToString("X");
                    textBox2.Text = String.Join(Environment.NewLine, addressInfo.Offsets.Select(_ => _.HexValue));
                }
                else
                {
                    textBox1.Text = String.Empty;
                    textBox2.Text = String.Empty;
                }
            }
        }

        #endregion

        #region Тест чтения

        private void button3_Click(object sender, EventArgs e)
        {
            var games = Storage.GetItems<GameInfo>();
            var processes = games
                .Select(_ => Process.GetProcessesByName(_.ProcessName)
                    .FirstOrDefault()?.ProcessName)
                .Where(_ => _ != null)
                .ToArray();

            listBox4.Items.Clear();
            listBox4.Items.AddRange(processes);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var item = (String)listBox4.SelectedItem;
            using (var reader = new ProcessReader(item))
            {
                if (_games.TryGetValue(item, out var addressInfo))
                {
                    textBox6.Text += $"{reader.Read<Single>(addressInfo)}{Environment.NewLine}";
                }
                else
                {
                    _logger.Warn($"Can't get game with name {item}");
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBox6.Clear();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (listBox4.SelectedItem != null)
            {
                _reader = new ProcessReader((String)listBox4.SelectedItem);
                timer1.Start();
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            _reader.Dispose();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            var item = (String)listBox4.SelectedItem;
            if (_games.TryGetValue(item, out var addressInfo))
            {
                textBox6.Text += $"{_reader.Read<Single>(addressInfo)}{Environment.NewLine}";
            }
            else
            {
                _logger.Warn($"Can't get game with name {item}");
            }
        }

        #endregion

        #region Порты

        private void button8_Click(object sender, EventArgs e)
        {
            var ports = PortHelper.GetAvailableComPorts();
            var text = String.Empty;

            foreach(var port in ports)
            {
                text += port.ToString();
            }
            textBox5.Text = text;
        }

        #endregion

        #region Окно
        private void Form1_Load(object sender, EventArgs e)
        {
            if (File.Exists(_keysFileName))
            {
                using (var reader = new JsonFileReader(new SourceFileInfo() { Path = _keysFileName }))
                {
                    var keys = reader.ReadAll<KeyString>();
                    Storage.SetItems(keys);
                    UpdateKeysList();
                    UpdateKeysTree();
                }
            }

            if (File.Exists(_devicesFileName))
            {
                using (var reader = new JsonFileReader(new SourceFileInfo() { Path = _devicesFileName }))
                {
                    var devices = reader.ReadAll<DeviceInfo>();
                    Storage.SetItems(devices);
                    UpdateDevicesList();
                }
            }

            if (File.Exists(_gamesFileName))
            {
                using (var reader = new JsonFileReader(new SourceFileInfo() { Path = _gamesFileName }))
                {
                    var games = reader.ReadAll<GameInfo>();
                    Storage.SetItems(games);
                    UpdateGamesList();
                }
            }
        }

        #endregion

        #region Updaters

        public void UpdateDevicesList()
        {
            var devices = Storage.GetItems<DeviceInfo>();
            foreach(var listbox in _devicesListBoxes)
            {
                listbox.Items.Clear();
                foreach(var device in devices)
                {
                    listbox.Items.Add(device);
                }

                listbox.DisplayMember = "Name";
            }
        }

        public void UpdateKeysTree()
        {
            treeView1.Nodes.Clear();

            var keys = Storage.GetItems<KeyString>();
            foreach(var key in keys)
            {
                var newNode = treeView1.Nodes.Add(key.Key);
                foreach(var alias in key.Aliases)
                {
                    newNode.Nodes.Add(alias);
                }
            }
        }

        public void UpdateKeysList()
        {
            var keys = Storage.GetItems<KeyString>();
            foreach (var listbox in _keysListBoxes)
            {
                listbox.Items.Clear();
                foreach (var key in keys)
                {
                    listbox.Items.Add(key);
                }

                listbox.DisplayMember = "Key";
            }
        }

        public void UpdateGamesList()
        {
            var games = Storage.GetItems<GameInfo>();
            foreach (var listbox in _gamesListBoxes)
            {
                listbox.Items.Clear();
                foreach (var game in games)
                {
                    listbox.Items.Add(game);
                }

                listbox.DisplayMember = "DisplayingName";
            }
        }


        #endregion

        #region Устройства

        private void button13_Click(object sender, EventArgs e)
        {
            var device = new DeviceInfo(textBox10.Text, textBox9.Text);
            Storage.AddItem(device);
            UpdateDevicesList();
        }

        #endregion

        #region Настройка

        private void listBox6_SelectedIndexChanged(object sender, EventArgs e)
        {
            Storage.SetSelectedItem(listBox6.SelectedItem as DeviceInfo);
        }

        private void listBox7_SelectedIndexChanged(object sender, EventArgs e)
        {
            Storage.SetSelectedItem(listBox7.SelectedItem as KeyString);
        }

        private void button15_Click(object sender, EventArgs e)
        {
            var devices = Storage.GetItems<DeviceInfo>();
            using (var writer = new JsonFileWriter(new SourceFileInfo() { Path = _devicesFileName }))
            {
                writer.WriteAll(devices);
            }
        }

        #endregion

        #region Consts

        private ProcessReader _reader;
        private Dictionary<String, AddressInfo> _games;

        private readonly ListBox[] _keysListBoxes = new ListBox[0];
        private readonly ListBox[] _devicesListBoxes = new ListBox[0];
        private readonly ListBox[] _gamesListBoxes = new ListBox[0];
        private readonly ListBox[] _deviceKeysListBoxes = new ListBox[0];

        private const string _devicesFileName = "devices.gext";
        private const string _keysFileName = "keys.gext";
        private const string _gamesFileName = "games.gext";

        private readonly ILogger _logger = LogManager.GetCurrentClassLogger();

        #endregion
    }
}
