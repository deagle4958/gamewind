﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Base.Storage
{
    public class KeyString : IStorageItem
    {
        public KeyString()
        {
            Key = String.Empty;
            Aliases = new List<String>();
        }

        public KeyString(String key) : this()
        {
            Key = key;
        }

        public KeyString(String key, params String[] aliases) : this(key)
        {
            Aliases.AddRange(aliases);
        }

        public KeyString(String key, List<String> aliases) : this(key)
        {
            Aliases = aliases;
        }

        /// <summary>
        /// Ключ
        /// </summary>
        public String Key { get; set; }

        /// <summary>
        /// Алиасы ключа
        /// </summary>
        public List<String> Aliases { get; set; }
    }
}
