﻿using DynamicExpresso;
using GameWind.Parse.Expressions.Condition;
using GameWind.Parse.Expressions.Range;
using GameWind.Parse.Parsing;
using MoreLinq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using static GameWind.Parse.Parsing.ParseConsts;

namespace GameWind.Parse.Expressions
{
    public class Expression
    {
        public Expression(String expressionText)
        {
            SourceText = expressionText;
            _parseText = expressionText;

            Parameters = RegExpressions.VariableRegex
                .Matches(SourceText)
                .Cast<Match>()
                .Select(_ => new Parameter(_.Result("$1"), typeof(Double)))
                .DistinctBy(_ => _.Name)
                .ToArray();

            var previouslyCalcs = new List<IPreviouslyCalcExpression>();

            previouslyCalcs.AddRange(ExpressionHelper.CreateAll<RangeExpression>(
                RegExpressions.RangeRegex,
                Identifiers.RangeIdentifier,
                Parameters,
                ref _parseText));

            previouslyCalcs.AddRange(ExpressionHelper.CreateAll<IfElseExpression>(
                RegExpressions.IfElseRegex,
                Identifiers.IfElseIdentifier,
                Parameters,
                ref _parseText));

            previouslyCalcs.ForEach(_ => _.UpdateExpressionFromMainExpressionText(_parseText, true));

            PreviouslyCalcs = previouslyCalcs.ToArray();
        }

        public Double Calc(params Object[] args)
        {
            var interpreter = new Interpreter();

            var resultExprText = _parseText;
            var previouslyCalcGroups = PreviouslyCalcs
                .GroupBy(_ => _.CalcOrder)
                .OrderBy(_ => _.Key);

            foreach (var calcs in previouslyCalcGroups)
            {
                foreach (var calc in calcs)
                {
                    // TODO для вычислений нулевой группы можно не обновлять выражения
                    calc.UpdateExpressionFromMainExpressionText(resultExprText);
                    var exprCalcResult = calc.CalcString(args);

                    var exprRegex = calc.GetExpressionRegex();
                    resultExprText = exprRegex.Replace(resultExprText, exprCalcResult);
                }
            }

            resultExprText = resultExprText
                .Replace("[", String.Empty)
                .Replace("]", String.Empty);

            var resultExpr = interpreter.Parse(resultExprText, Parameters);
            return resultExpr.InvokeDouble(args);
        }

        /// <summary>
        /// Текст промежуточного выражения
        /// </summary>
        public String ParseText => _parseText;

        /// <summary>
        /// Текст исходного выражения
        /// </summary>
        public String SourceText { get; }

        /// <summary>
        /// Список параметров выражения
        /// </summary>
        public Parameter[] Parameters { get; }

        /// <summary>
        /// Ключи использующихся значений
        /// </summary>
        public IPreviouslyCalcExpression[] PreviouslyCalcs { get; }

        private readonly String _parseText;
    }
}
