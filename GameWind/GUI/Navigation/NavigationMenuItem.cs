﻿using Prism.Commands;
using Prism.Regions;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace GameWind.GUI.Navigation
{
    public class NavigationMenuItem : INavigationMenuItem
    {
        public Uri SectionUri { get; set; }
        
        public String SectionName { get; set; }
        
        public ObservableCollection<NavigationMenuItem> Items { get; set; }

        public ICommand ShowSectionCommand { get; private set; }

        public NavigationMenuItem(IRegionManager regionManager, String sectionName, Uri sectionUri)
        {
            _regionManager = regionManager;
            SectionName = sectionName;
            SectionUri = sectionUri;

            ShowSectionCommand = new DelegateCommand(ShowSection);
        }

        public void ShowSection()
        {
            _regionManager.RequestNavigate(ContentRegions.MainContentRegion, SectionUri);
        }

        private IRegionManager _regionManager;
    }
}
