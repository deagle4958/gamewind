﻿using DynamicExpresso;
using GameWind.Parse.Expressions.Condition;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameWind.Tests.Parse
{
    public class IfElseParseTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void IfElseExpressionTest()
        {
            var exprText = @"
            If ([Speed] < [MaxSpeed])
            {
                [SpeedKph]
            }
            else
            {
                [SpeedMph]
            }";
            var expr = new IfElseExpression("Test", exprText, _parameters);

            Assert.AreEqual("[SpeedKph]", expr.TrueExprText);
            Assert.AreEqual("[SpeedMph]", expr.FalseExprText);

            var result1 = expr.Calc(50, 100, 50, 70);
            var result2 = expr.Calc(110, 100, 50, 70);
            Assert.AreEqual("[SpeedKph]", result1);
            Assert.AreEqual("[SpeedMph]", result2);
        }

        private readonly Parameter[] _parameters = new[]
        {
            new Parameter("Speed", typeof(Double)),
            new Parameter("MaxSpeed", typeof(Double)),
            new Parameter("SpeedKph", typeof(Double)),
            new Parameter("SpeedMph", typeof(Double)),
        };
    }
}
