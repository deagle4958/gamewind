﻿using Base.ComPort;
using NLog;
using System;
using System.Collections.Generic;
using System.Management;
using System.Text;
using System.IO.Ports;
using Base.Writing;

namespace GameWind.Writers.ComPortWriting
{
    public class ComPortWriter : IWriter<ComPortWritingInfo>, IDisposable
    {
        public ComPortWriter(ComPortDeviceInfo info)
        {
            _serialPort = new SerialPort(info.DeviceId)
            {
                ReadTimeout = _timeout,
                WriteTimeout = _timeout,
                BaudRate = _baudRate,
            };
        }

        public void Dispose()
        {
            _serialPort.Close();
            _serialPort = null;
        }

        public virtual void Write<T>(T data, ComPortWritingInfo writingInfo = null)
        {
            var dataString = data.ToString();
            _serialPort.WriteLine(dataString);
        }

        private const Int32 _baudRate = 9600;
        private const Int32 _timeout = 500;
        private SerialPort _serialPort;
        private static readonly ILogger _logger = LogManager.GetCurrentClassLogger();
    }
}
