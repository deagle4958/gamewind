﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Expressions
{
    public class Range
    {
        public Range()
        {
        }

        public Range(Double inMin, Double inMax)
        {
            InMin = inMin;
            InMax = inMax;
        }

        public Range(Double inMin, Double inMax, Double outMin, Double outMax) : this(inMin, inMax)
        {
            OutMin = outMin;
            OutMax = outMax;
        }

        public Double Calc(Double value)
        {
            if (value >= InMax)
            {
                if (AutoIn)
                {
                    InMax = value;
                }
                return OutMax;
            }
            if (value <= InMin)
            {
                if (AutoIn)
                {
                    InMin = value;
                }
                return OutMin;
            }

            var calcValue = (value - InMin) * (OutMax - OutMin) / (InMax - InMin) + OutMin;
            return calcValue;
        }

        /// <summary>
        /// Минимальное значение, получаемое на входе
        /// </summary>
        public Double InMin { get; set; }

        /// <summary>
        /// Максимальное значение, получаемое на входе
        /// </summary>
        public Double InMax { get; set; }

        /// <summary>
        /// Минимальное значение, отправляемое на выход
        /// </summary>
        public Double OutMin { get; set; }

        /// <summary>
        /// Максимальное значение, отправляемое на выход
        /// </summary>
        public Double OutMax { get; set; }

        /// <summary>
        /// Автоматическая настройка входящих параметров
        /// </summary>
        public Boolean AutoIn { get; set; }
    }
}
