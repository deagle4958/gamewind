﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace GameWind.GUI.Helpers
{
    public static class GridDependencyProperty
    {
        public static DependencyProperty RowCountProperty = 
            DependencyProperty.RegisterAttached("RowCount", typeof(Int32), typeof(GridDependencyProperty), new PropertyMetadata(0, OnRowCountChanged));

        public static void SetRowCount(DependencyObject obj, Int32 value)
        {
            obj.SetValue(RowCountProperty, value);
        }

        public static Int32 GetRowCount(DependencyObject obj)
        {
            return (Int32)obj.GetValue(RowCountProperty);
        }

        private static void OnRowCountChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var newVal = (Int32)e.NewValue;
            if (obj is Grid grid && newVal > 0)
            {
                grid.RowDefinitions.Clear();

                for (var i = 0; i < newVal; ++i)
                {
                    grid.RowDefinitions.Add(new RowDefinition());
                }
            }
        }

        public static readonly DependencyProperty ColumnCountProperty = 
            DependencyProperty.RegisterAttached("ColumnCount", typeof(Int32), typeof(GridDependencyProperty), new PropertyMetadata(0, ColumnCountChanged));

        public static Int32 GetColumnCount(DependencyObject obj)
        {
            return (Int32)obj.GetValue(ColumnCountProperty);
        }

        public static void SetColumnCount(DependencyObject obj, Int32 value)
        {
            obj.SetValue(ColumnCountProperty, value);
        }

        public static void ColumnCountChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var newVal = (Int32)e.NewValue;
            if (obj is Grid grid && newVal > 0)
            {
                grid.ColumnDefinitions.Clear(); 
                for (int i = 0; i < newVal; ++i)
                {
                    grid.ColumnDefinitions.Add(new ColumnDefinition());
                }
            }
        }
    }
}
