﻿using Base.ViewModels;
using Prism.Unity;
using System;
using Unity;

namespace GameWind.MVVM.Tools
{
    public class ViewModelLocator
    {
        public Type Type { get; set; }

        public Object ViewModel
        {
            get
            {
                if (this.unityContainer == null)
                {
                    Console.WriteLine("<!> Can't find container!");
                    return null;
                }

                if(Type != null && unityContainer.IsRegistered(Type))
                {
                    return unityContainer.Resolve(Type);
                }
                else
                {
                    Console.WriteLine("<!> Can't find view model '{0}'", Type?.Name);
                    return null;
                }
            }
        }

        public ViewModelLocator()
        {
            PrismApplication prismApp = App.Current as PrismApplication;
            this.unityContainer = prismApp.Container.GetContainer();
        }

        private readonly IUnityContainer unityContainer;
    }
}
