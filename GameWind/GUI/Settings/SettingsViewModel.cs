﻿using GameWind.MVVM;
using MaterialDesignThemes.Wpf;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GameWind.GUI.Settings
{
    public class SettingsViewModel : ViewModelBase
    {
        public ObservableCollection<AppTheme> AllowedThemes { get; private set; }
        
        public ICommand ApplyCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public AppTheme SelectedTheme
        {
            get => _selectedTheme;
            set
            {

                _selectedTheme = value;
                if(value != null)
                {
                    _paletteHelper.SetTheme(value);
                }
                RaisePropertyChanged(() => SelectedTheme);
            }
        }

        public SettingsViewModel(PaletteHelper paletteHelper)
        {
            _paletteHelper = paletteHelper;
            _currentTheme = paletteHelper.GetTheme() as AppTheme;

            ApplyCommand = new DelegateCommand(OnApply, CanExecuteApply).ObservesProperty(()=> SelectedTheme);
            CancelCommand = new DelegateCommand(OnCancel);

            AllowedThemes = new ObservableCollection<AppTheme>
            {
                AppTheme.DefaultTheme,
                AppTheme.DarkTheme,
            };
        }

        private Boolean CanExecuteApply()
        {
            return SelectedTheme != null;
        }

        private void OnApply()
        {
            _currentTheme = _selectedTheme;
            //Добавить сохранение в настройки пользователя
            DialogHost.CloseDialogCommand.Execute(null, null);
        }

        private void OnCancel()
        {
            _paletteHelper.SetTheme(_currentTheme);
            DialogHost.CloseDialogCommand.Execute(null, null);
        }

        private readonly PaletteHelper _paletteHelper;
        private AppTheme _selectedTheme;
        private AppTheme _currentTheme;
    }
}
