﻿using Base.Process;
using Base.Reading;
using Base.Storage;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Game
{
    public class GameInfo : IStorageItem
    {
        public GameInfo()
        {
            GameAddresses = new Dictionary<String, AddressInfo>();
        }

        public GameInfo(String displayingName, String processName) : this()
        {
            DisplayingName = displayingName;
            ProcessName = processName;
        }

        public GameInfo(String displayingName, String processName, Dictionary<String, AddressInfo> gameAddresses) : this(displayingName, processName)
        {
            GameAddresses = gameAddresses;
        }

        /// <summary>
        /// Попытка добавить новый адрес
        /// </summary>
        /// <param name="key">Ключ</param>
        /// <param name="addressInfo">Адрес</param>
        /// <returns>Успешно ли был добавлен новый адрес (false, если в данной группе уже существует такой адрес)</returns>
        public Boolean TryAddNewAddress(String key, AddressInfo addressInfo)
        {
            if (!GameAddresses.ContainsKey(key))
            {
                GameAddresses.Add(key, addressInfo);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Добавление нового адреса с ключом (если ключ существует - заменяет значение, если нет - создаёт новую запись)
        /// </summary>
        /// <param name="key">Ключ</param>
        /// <param name="addressInfo">Адрес</param>
        public void SetOrAddAddress(String key, AddressInfo addressInfo)
        {
            if (GameAddresses.ContainsKey(key))
            {
                GameAddresses[key] = addressInfo;
            }
            else
            {
                GameAddresses.Add(key, addressInfo);
            }
        }

        /// <summary>
        /// Отображаемое имя
        /// </summary>
        public String DisplayingName { get; set; }

        /// <summary>
        /// Имя процесса
        /// </summary>
        public String ProcessName { get; set; }

        /// <summary>
        /// Список поддерживаемых ключей адресов
        /// </summary>
        [JsonIgnore]
        public String[] SupportedAddressKeys
        {
            get { return GameAddresses.Keys.ToArray(); }
        }

        /// <summary>
        /// Список привязанных к игре возможных поинтеров
        /// </summary>
        public Dictionary<String, AddressInfo> GameAddresses { get; }
    }
}
