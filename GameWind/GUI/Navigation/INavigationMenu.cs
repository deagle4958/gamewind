﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameWind.GUI.Navigation
{
    public interface INavigationMenu
    {
        IEnumerable<INavigationMenuItem> Items { get; }
    }
}
