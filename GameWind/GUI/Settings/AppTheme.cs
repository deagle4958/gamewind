﻿using MaterialDesignColors;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace GameWind.GUI
{
    public sealed class AppTheme : Theme
    {
        public Int32 Id { get; set; }

        public String Name { get; set; }

        public AppTheme(Boolean isDark = false)
        {
            this.SetBaseTheme(isDark ? (IBaseTheme)new MaterialDesignDarkTheme() : new MaterialDesignLightTheme());

            var notSetColor = (Color)ColorConverter.ConvertFromString("#27D507");
            var notSetColorPair = new ColorPair(notSetColor, Colors.OrangeRed);
            this.Background = notSetColor;
            this.Body = notSetColor;
            this.BodyLight = notSetColor;
            this.CardBackground = notSetColor;
            this.CheckBoxDisabled = notSetColor;
            this.CheckBoxOff = notSetColor;
            this.ChipBackground = notSetColor;
            this.ColumnHeader = notSetColor;
            this.Divider = notSetColor;
            this.FlatButtonClick = notSetColor;
            this.FlatButtonRipple = notSetColor;
            this.Paper = notSetColor;
            this.PrimaryDark = notSetColorPair;
            this.PrimaryLight = notSetColorPair;
            this.PrimaryMid = notSetColorPair;
            this.SecondaryDark = notSetColorPair;
            this.SecondaryLight = notSetColorPair;
            this.SecondaryMid = notSetColorPair;
            this.Selection = notSetColor;
            this.SnackbarBackground = notSetColor;
            this.SnackbarMouseOver = notSetColor;
            this.SnackbarRipple = notSetColor;
            this.TextAreaBorder = notSetColor;
            this.TextAreaInactiveBorder = notSetColor;
            this.TextFieldBoxBackground = notSetColor;
            this.TextFieldBoxDisabledBackground = notSetColor;
            this.ToolBarBackground = notSetColor;
            this.ToolTipBackground = notSetColor;
            this.ValidationError = notSetColor;           
        }

        public static AppTheme DefaultTheme = new AppTheme
        {
            Id = 0,
            Name = "Default",
            Background = Colors.White,
            CardBackground = Colors.White,
            PrimaryDark = new ColorPair((Color)ColorConverter.ConvertFromString("#0d47a1"), Colors.White),
            PrimaryMid = new ColorPair((Color)ColorConverter.ConvertFromString("#1565c0"), Colors.White),
            PrimaryLight = new ColorPair((Color)ColorConverter.ConvertFromString("#2196f3"), Colors.White),
            ValidationError = Colors.DarkRed,
            //Drawers background
            Paper = Colors.White,
            FlatButtonRipple = (Color)ColorConverter.ConvertFromString("#90caf9"),
            FlatButtonClick = (Color)ColorConverter.ConvertFromString("#bbdefb"),
            Body = Colors.Black,
            SecondaryMid = new ColorPair((Color)ColorConverter.ConvertFromString("#1565c0"), Colors.White),
        };

        public static AppTheme DarkTheme = new AppTheme(true)
        {
            Id = 1,
            Name = "Spotify",
            Background = (Color)ColorConverter.ConvertFromString("#191414"),
            CardBackground = (Color)ColorConverter.ConvertFromString("#272727"),
            PrimaryDark = new ColorPair((Color)ColorConverter.ConvertFromString("#1db954"), Colors.White),
            PrimaryMid = new ColorPair((Color)ColorConverter.ConvertFromString("#1db954"), Colors.White),
            PrimaryLight = new ColorPair((Color)ColorConverter.ConvertFromString("#1db954"), Colors.White),
            ValidationError = Colors.DarkRed,
            //Drawers background
            Paper = (Color)ColorConverter.ConvertFromString("#191414"),
            FlatButtonRipple = (Color)ColorConverter.ConvertFromString("#555"),
            FlatButtonClick = (Color)ColorConverter.ConvertFromString("#424242"),
            Body = Colors.LightGray,
            SecondaryMid = new ColorPair((Color)ColorConverter.ConvertFromString("#1db954"), Colors.White),
            SecondaryLight = new ColorPair((Color)ColorConverter.ConvertFromString("#1db954"), Colors.White),
        };
    }
}
