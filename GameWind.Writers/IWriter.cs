﻿using Base.Writing;
using System;

namespace GameWind.Writers
{
    public interface IWriter<Info> where Info : class, IWriteInfo
    {
        void Write<T>(T data, Info writeInfo = null);
    }
}
