﻿using Base.Reading;
using Base.Writing;
using System;
using System.Collections.Generic;
using System.Text;

namespace Base.File
{
    public class SourceFileInfo : IReadInfo, IWriteInfo
    {
        /// <summary>
        /// Путь к файлу
        /// </summary>
        public String Path { get; set; }
    }
}
