﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using static GameWind.Parse.Parsing.ParseConsts.RegExpressions;

namespace GameWind.Tests.Parse
{
    public class RegexParseTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void RangeRegexTest()
        {
            var t1 = "Range ([Speed])";
            var t2 = "Range ([Speed], _, _, 0, [ArduinoMaxValue])";
            var t3 = "Range ([Speed], 0, [MaxSpeed])";
            var t4 = "Range ([Speed], 0, [MaxSpeed], 0, [ArduinoMaxValue])";

            Assert.IsTrue (RangeRegex.IsMatch(t1));
            Assert.IsTrue (RangeRegex.IsMatch(t2));
            Assert.IsTrue (RangeRegex.IsMatch(t3));
            Assert.IsTrue (RangeRegex.IsMatch(t4));
        }

        [Test]
        public void IfElseRegexTest()
        {
            var t1 = "If ([Speed] > 100) {[Speed]} else {0}";
            var t2 = "If ([Speed] > 100) {[Speed]}";
            var t3 = @"If ([Speed > 0] || [Speed < 100])
            {
                [Speed]
            }";
            var t4 = @"If ([Speed] < [MaxSpeed])
            {
                [SpeedKph]
            }
            else
            {
                [SpeedMph]
            }";

            Assert.IsTrue(IfElseRegex.IsMatch(t1));
            Assert.IsTrue(IfElseRegex.IsMatch(t2));
            Assert.IsTrue(IfElseRegex.IsMatch(t3));
            Assert.IsTrue(IfElseRegex.IsMatch(t4));
        }
    }
}
