﻿using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameWind.GUI.Navigation
{
    public class NavigationMenu : INavigationMenu
    {
        public IEnumerable<INavigationMenuItem> Items { get; private set; }

        public NavigationMenu(IRegionManager regionManager)
        {
            Items = new List<INavigationMenuItem>
            {
                new NavigationMenuItem(regionManager, SectionsUri.TestSection, SectionsUri.TestSectionUri)
                {
                    Items = new ObservableCollection<NavigationMenuItem>
                    {
                         new NavigationMenuItem(regionManager, SectionsUri.TestSection, SectionsUri.TestSectionUri),
                         new NavigationMenuItem(regionManager, SectionsUri.TestSection, SectionsUri.TestSectionUri),
                    }
                },
                new NavigationMenuItem(regionManager, SectionsUri.TestView1, SectionsUri.TestView1Uri),
            };
        }
    }
}
