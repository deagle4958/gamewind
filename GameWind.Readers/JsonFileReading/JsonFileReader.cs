﻿using Base.File;
using Base.Game;
using Base.Reading;
using Base.Writing;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace GameWind.Readers.JsonFileReading
{
    public class JsonFileReader : IReader<IReadInfo>, IDisposable
    {
        public JsonFileReader(SourceFileInfo fileInfo)
        {
            _streamReader = new StreamReader(fileInfo.Path);
        }

        public void Dispose()
        {
            _streamReader.Dispose();
            _streamReader = null;
        }

        public T Read<T>(IReadInfo readInfo = null)
        {
            var jsonData = _streamReader.ReadLine();
            var data = JsonConvert.DeserializeObject<T>(jsonData);
            return data;
        }

        public IEnumerable<T> ReadAll<T>(IReadInfo readInfo = null)
        {
            var jsonDataArray = _streamReader
                .ReadToEnd()
                .Split(new Char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries)
                .ToArray();

            var data = jsonDataArray
                .Select(_=> JsonConvert.DeserializeObject<T>(_));

            return data;
        }

        private StreamReader _streamReader;

    }
}
