﻿using Base.File;
using Base.Game;
using Base.Writing;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace GameWind.Writers.JsonFileWriting
{
    public class JsonFileWriter : IWriter<IWriteInfo>, IDisposable
    {
        public JsonFileWriter(SourceFileInfo fileInfo)
        {
            _streamWriter = new StreamWriter(fileInfo.Path);
        }

        public void Dispose()
        {
            _streamWriter.Dispose();
            _streamWriter = null;
        }

        public void Write<T>(T writeData, IWriteInfo writeInfo = null)
        {
            var jsonData = JsonConvert.SerializeObject(writeData);
            _streamWriter.WriteLine(jsonData);
        }

        public void WriteAll<T>(IEnumerable<T> dataArray)
        {
            var jsonDataArray = dataArray
                .Select(_ => JsonConvert.SerializeObject(_));

            foreach(var jsonData in jsonDataArray)
            {
                _streamWriter.WriteLine(jsonData);
            }
        }

        private StreamWriter _streamWriter;
    }
}
