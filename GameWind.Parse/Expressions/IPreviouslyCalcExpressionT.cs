﻿using DynamicExpresso;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameWind.Parse.Expressions
{
    public interface IPreviouslyCalcExpressionT<T> : IPreviouslyCalcExpression
    {
        T Calc(params Object[] args);
    }
}
