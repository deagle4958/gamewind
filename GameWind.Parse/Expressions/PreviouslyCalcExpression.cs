﻿using DynamicExpresso;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using static GameWind.Parse.Parsing.ParseConsts;
using static GameWind.Parse.Parsing.ParseConsts.RegExpressions;

namespace GameWind.Parse.Expressions
{
    public abstract class PreviouslyCalcExpression : IPreviouslyCalcExpression
    {
        public virtual void Initialize(String name, params Parameter[] parameters)
        {
            _name = name;
            _parameters = parameters;
        }

        public virtual void UpdateExpressionFromMainExpressionText(String mainExpressionText, Boolean initCalcOrder = false)
        {
            var exprRegex = GetExpressionRegex();
            var expression = exprRegex.Match(mainExpressionText).Result("${expr}");

            UpdateExpression(expression);

            if(initCalcOrder)
            {
                _calcOrder = GetCalcOrder(expression);
            }
        }

        public virtual Int32 GetCalcOrder(String expression)
        {
            return SubExpressionsRegex
                    .Matches(expression)
                    .Count;
        }

        public virtual void UpdateExpression(String expressionText)
        {
            throw new NotImplementedException();
        }

        public virtual String CalcString(params Object[] args)
        {
            throw new NotImplementedException();
        }

        public virtual Regex GetExpressionRegex()
        {
            var exprRegex = new Regex($"{ExpressionStartRegex}{_name}(?<expr>[\\s\\S]*){_name}{ExpressionEndRegex}");
            return exprRegex;
        }

        public String Name => _name;
        public Int32 CalcOrder => _calcOrder;

        protected Parameter[] _parameters;
        protected Int32 _calcOrder = 1;
        protected String _name;
    }
}
